FROM ubuntu:16.04
#docker build -t example/book-list-frontend .
#docker run -it --rm -p 4200:4200 example/book-list-frontend

USER root

#Install curl
RUN apt-get update \
    && apt-get -y install curl

#Install sudo command
RUN apt-get update && \
      apt-get -y install sudo

RUN mkdir /home/app/
WORKDIR /home/app/

ARG ANGULAR_CLI_VERSION=1.3.2

#Install node and angular cli
RUN curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
RUN apt-get update && apt-get install -y nodejs
RUN npm install -g "@angular/cli@$ANGULAR_CLI_VERSION"

COPY package.json /home/app/package.json
COPY .angular-cli.json /home/app/.angular-cli.json
COPY karma.conf.js /home/app/karma.conf.js
COPY tsconfig.json /home/app/tsconfig.json
COPY src /home/app/src
COPY typings-books-list /home/app/typings-books-list
RUN npm install

EXPOSE 4200
CMD ["ng", "serve", "--disable-host-check", "--host", "0.0.0.0"]
