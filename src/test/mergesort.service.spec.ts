import { SortService } from '../app/services/mergesort.service';
import * as moment from 'moment';

class MockCompoenent { }

describe('MergeSortService', () => {

  let sortService: SortService;

  beforeEach((() => {
    sortService = new SortService();
  }));

  it('should sort when already sorted array has fewer elements than original not sorted array', (() => {
    const notSortedBooks = getBooks(6);
    const arrayBooksSpy = spyOn(notSortedBooks, 'sort').and.callThrough();
    const sortedBooks = sortService.sortBy(notSortedBooks, 'up_title');
    expect(sortedBooks.length).toBe(6);
    expect(notSortedBooks.sort).toHaveBeenCalledTimes(1);
  }));

  it('should not call sort when already sorted array has the same elements than original not sorted array', (() => {
    const notSortedBooks = getBooks(6);
    sortService.title_down_books = notSortedBooks;
    const arrayBooksSpy = spyOn(notSortedBooks, 'sort');
    const sortedBooks = sortService.sortBy(notSortedBooks, 'up_title');
    expect(sortedBooks.length).toBe(6);
    expect(notSortedBooks.sort).not.toHaveBeenCalled();
  }));


});

const getBooks = function (num: number) {
  const books: Book[] = [];
  for (let i = 0; i < num; i++) {
    books.push({
      name: 'name' + i,
      author: { name: 'name_auth_' + i, gender: 'male' },
      genre: 'Finance',
      published: moment().utc().add(-i % 1000, 'days').toDate()
    });
  }
  return books;
};
