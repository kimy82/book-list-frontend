import { BooksRetrieverService } from '../app/services/booksretriever.service';
import * as moment from 'moment';

class MockCompoenent { }

describe('BooksRetrieverService', () => {

  let booksRetrieverService: BooksRetrieverService;
  const  booksSorted: Map<SortDirection, Book[]> = new Map<SortDirection, Book[]>();

  beforeEach((() => {
    booksRetrieverService = new BooksRetrieverService();

    booksSorted.set('none', []);
    booksSorted.set('up_title', []);
    booksSorted.set('down_title', []);
    booksSorted.set('up_author', []);
    booksSorted.set('down_author', []);
  }));

  it('should return the same lists when no data is present in http progress event', (() => {
    const chunkedBooks = booksRetrieverService.getChunckOfBooks({ type: 3, partialText: '', loaded: 0 }, booksSorted, []);
    expect(chunkedBooks.booksDisplayed.length).toBe(0);
  }));

  it('should build books and displayed books array when is first chunck of data and chunck is a correct json', (() => {
    const chunkedBooks = booksRetrieverService.getChunckOfBooks({ type: 3, partialText: JSON.stringify(getBooks(3)), loaded: 0 }, booksSorted, []);
    expect(chunkedBooks.booksDisplayed.length).toBe(3);
  }));

  it('should build books and displayed books array when is first chunck of data and chunck is not a correct json', (() => {
    const jsonCorrectString = JSON.stringify(getBooks(3));

    const chunkedBooks = booksRetrieverService.getChunckOfBooks({ type: 3, partialText: jsonCorrectString.substring(0, jsonCorrectString.length - 6), loaded: 0 }, booksSorted, []);
    expect(chunkedBooks.booksDisplayed.length).toBe(2);
  }));

  it('should add new books when the second chunck of data arrives', (() => {
    const jsonCorrectString = JSON.stringify(getBooks(3));

    const chunkedBooks = booksRetrieverService.getChunckOfBooks({ type: 3, partialText: jsonCorrectString.substring(0, jsonCorrectString.length - 6), loaded: 0 }, booksSorted, []);
    expect(chunkedBooks.booksDisplayed.length).toBe(2);

    const jsonCorrectStringSecondChunck = JSON.stringify(getBooks(7));
    const secondChunkedBooks = booksRetrieverService.getChunckOfBooks(
      { type: 3, partialText: jsonCorrectStringSecondChunck.substring(0, jsonCorrectStringSecondChunck.length - 6), loaded: 0 },
      chunkedBooks.books, chunkedBooks.booksDisplayed);

    expect(secondChunkedBooks.booksDisplayed.length).toBe(6);
  }));

});

const getBooks = function (num: number) {
  const books: Book[] = [];
  for (let i = 0; i < num; i++) {
    books.push({
      name: 'name' + i,
      author: { name: 'name_auth_' + i, gender: 'male' },
      genre: 'Finance',
      published: moment().utc().add(-i % 1000, 'days').toDate()
    });
  }
  return books;
};
