import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from '../app/app.component';
import { BooksListComponent } from '../app/books-list/books-list.component';

import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BooksService } from '../app/services/books.service';
import { SortService } from '../app/services/mergesort.service';

class MockCompoenent {}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        BooksListComponent
      ],
      imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule
      ],
      providers: [
        SortService,
        {
        provide: BooksService,
        useFactory: () => new MockCompoenent()
      }],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    expect(fixture.nativeElement.querySelector('h1').innerHTML).toContain('Welcome to Books list');
  }));
});

