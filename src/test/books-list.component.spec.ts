import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import { MockBackend, MockConnection } from '@angular/http/testing';
import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';

import * as moment from 'moment';

import { BooksListComponent } from '../app/books-list/books-list.component';
import { BooksService } from '../app/services/books.service';
import { SortService } from '../app/services/mergesort.service';


class BooksServiceMock {
  getBooks(): Observable<any> {
    return Observable.of(this._commonValidData());
  }

  _commonValidData(): any {
    const books: Book[] = [];
    for (let i = 0; i <= 9997; i++) {
      books.push({
        name: 'name' + i,
        author: { name: 'name_auth_' + i, gender: 'male' },
        genre: 'Finance',
        published: moment().utc().add(-i % 1000, 'days').toDate()
      });
    }
    books.push({
      name: 'Fields of JAVA',
      author: { name: 'name_Casumo_', gender: 'male' },
      genre: 'Horror',
      published: moment().utc().add(-1000, 'days').toDate()
    });
    books.push({
      name: 'Another language',
      author: { name: 'Casumo_auth_', gender: 'male' },
      genre: 'Finance',
      published: moment().utc().add(-800, 'days').toDate()
    });
    books.push({
      name: 'Clean code',
      author: { name: 'name_auth_', gender: 'male' },
      genre: 'Finance',
      published: moment().utc().add(-90, 'days').toDate()
    });
    return { type: 3, partialText: JSON.stringify(books) };
  }
}

describe('BooksListComponent & booksretriever', () => {
  let component: BooksListComponent;
  let fixture: ComponentFixture<BooksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BooksListComponent
      ],
      imports: [
        FormsModule,
        BrowserModule,
        HttpClientModule
      ],
      providers: [
        SortService,
        {
          provide: BooksService,
          useFactory: () => new BooksServiceMock()
        }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve all books from the backend', async(() => {
    expect(component.booksSorted.get('none').length).toBe(10001);
  }));

  it('should filer by AuthorName', async(() => {
    component.authorName = 'Casumo';
    component.filterBooks();
    expect(component.booksDisplayed.length).toBe(2);
  }));

  it('should filter by BookName', async(() => {
    component.bookName = 'name7077';
    component.filterBooks();
    expect(component.booksDisplayed.length).toBe(1);
  }));

  it('should filter by Genre', async(() => {
    component.genresSelected.set('Horror', true);
    component.filterBooks();
    expect(component.booksDisplayed.length).toBe(1);
  }));

  it('should highlight Horror in Hallowen', async(() => {
    component.booksDisplayed = [];
    component.booksDisplayed.push({
      name: 'Hallowen',
      author: { name: 'name_auth_', gender: 'male' },
      genre: 'Horror',
      published: moment('2015-10-31 12:00').utc().toDate()
    });
    fixture.detectChanges();
    fixture.whenRenderingDone().then(() => {
      expect(fixture.nativeElement.querySelector('.halloween').className).toBe('book-item halloween');
    });
  }));

  it('should highlight Finance when is last friday of a month', async(() => {
    component.booksDisplayed = [];
    component.booksDisplayed.push({
      name: 'Las friday finance',
      author: { name: 'name_auth_', gender: 'male' },
      genre: 'Finance',
      published: moment('2017-09-29').utc().toDate()
    });
    fixture.detectChanges();
    fixture.whenRenderingDone().then(() => {
      expect(fixture.nativeElement.querySelector('.finance-last-friday').className).toBe('book-item finance-last-friday');
    });
  }));
});
