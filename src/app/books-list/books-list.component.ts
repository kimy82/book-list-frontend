import { Component, OnInit } from '@angular/core';
import { HttpEventType } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';

import * as moment from 'moment';
import { BooksService } from '../services/books.service';
import { BooksRetrieverService } from '../services/booksretriever.service';
import { SortService } from '../services/mergesort.service';

import * as swal from 'sweetalert';

@Component({
  selector: 'app-books-list',
  templateUrl: './books-list.component.html',
  styleUrls: ['./books-list.component.less']
})
export class BooksListComponent implements OnInit {

  private static october = 9;
  private static last_october_day = 31;
  private static friday = 5;
  public genres: Genre[] = ['Satire', 'Science fiction', 'Drama', 'Action', 'Romance', 'Mystery', 'Horror', 'Finance'];

  public booksSorted: Map<SortDirection, Book[]> = new Map<SortDirection, Book[]>();
  public booksDisplayed: Book[] = [];
  public booksFiltered: Book[] = [];

  public sorted: SortDirection = 'none';
  public filtering = true;

  public authorName = '';
  public bookName = '';
  public genre: Genre;
  public genresSelected: Map<Genre, boolean> = new Map<Genre, boolean>();
  public areMoreResults = true;

  constructor(private _bookService: BooksService, private _sortService: SortService) { }

  ngOnInit() {
    const retriever = new BooksRetrieverService(200);
    this._initialiseBooksMap();
    this._bookService.getBooks().subscribe(
      (event) => {
        if (event.type === HttpEventType.DownloadProgress) {
          const chunckResponse = retriever.getChunckOfBooks(event, this.booksSorted, this.booksDisplayed);
          this.booksSorted.set('none', chunckResponse.books.get('none'));
          if (this.booksDisplayed.length === 0) {
            this.booksDisplayed = chunckResponse.booksDisplayed;
          }
          this._coreBooksFilter(false);
        } else if (event.type === HttpEventType.Response) {
          console.log('Loading complete');
        }
      },
      (error) => swal('Oops...', 'Books are not available just now, try again later!', 'error')
    );
  }

  public filterBooks(): void {
    this.booksDisplayed = [];
    this._coreBooksFilter(true);
  }

  public filterBooksWithGenre(genre: Genre): void {
    this.genresSelected.set(genre, this.isGenreSelected(genre) ? false : true);
    this.filterBooks();
  }

  public isGenreSelected(genre: Genre): boolean {
    return this.genresSelected.get(genre);
  }

  public showMoreBooks(): void {
    this.booksDisplayed = this.booksDisplayed.concat(this.booksFiltered.slice(this.booksDisplayed.length, this.booksDisplayed.length + 20));
    if (this.booksDisplayed.length >= this.booksFiltered.length) {
      this.areMoreResults = false;
    }
  }

  public isHorrorAndHallowen(date: Date, genre: Genre): boolean {
    const bookMoment = moment(date);
    return bookMoment.month() === BooksListComponent.october
      && bookMoment.date() === BooksListComponent.last_october_day && genre === 'Horror';
  }

  public isFinanceAndLastFridayOfMonth(date: Date, genre: Genre): boolean {
    const bookMoment = moment(date);
    return bookMoment.day() === BooksListComponent.friday && bookMoment.date() >= (bookMoment.daysInMonth() - 6) && genre === 'Finance';
  }

  public sortBy(direction: SortDirection) {
    this.filtering = true;
    this.sorted = direction;
      setTimeout(() => {if (this.booksSorted.get('none').length > this.booksSorted.get(direction).length) {
          this.booksSorted.set(direction, [].concat(this._sortService.sortBy(this.booksSorted.get('none'), direction)));
      }
      this.filterBooks();
    }, 2);
  }

  private _getGenresAsString(): string {
    let genres = '';
    this.genresSelected.forEach((value, key) => { if (value) { genres += key + ','; } });
    return genres;
  }
  private _isMattchingAuthorName(book: Book): boolean {
    return this.authorName.trim().length < 1 || book.author.name.indexOf(this.authorName) !== -1;
  }

  private _isMattchingBookName(book: Book): boolean {
    return this.bookName.trim().length < 1 || book.name.indexOf(this.bookName) !== -1;
  }

  private _isMattchingGenre(genres: string, book: Book): boolean {
    return genres === '' || genres.indexOf(book.genre) !== -1;
  }

  private _initialiseBooksMap(): void {
    this.booksSorted.set('none', []);
    this.booksSorted.set('up_title', []);
    this.booksSorted.set('down_title', []);
    this.booksSorted.set('up_author', []);
    this.booksSorted.set('down_author', []);
  }

  private _coreBooksFilter(addToDisplayed: boolean): void {
    const genres = this._getGenresAsString();
    this.filtering = true;
    this.booksFiltered = this.booksSorted.get(this.sorted).filter(book => {
      if (this._isMattchingAuthorName(book) && this._isMattchingBookName(book) && this._isMattchingGenre(genres, book)) {
        if (this.booksDisplayed.length < 100 && addToDisplayed) {
          this.booksDisplayed.push(book);
        }
        return true;
      }
    });
    this.filtering = false;
  }

}
