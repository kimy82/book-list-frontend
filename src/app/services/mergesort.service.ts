import { HttpProgressEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';

@Injectable()
export class SortService {

    public static numberOfchuncks = 10;
    public author_down_books: Book[] = [];
    public title_down_books: Book[] = [];

    constructor() {}

    public sortBy(books, direction: SortDirection): Book[] {
        if (direction === 'up_author') {
            this._sortBooksByAuthor(books);
            return [].concat(this.author_down_books).reverse();
        } else if (direction === 'down_author') {
            this._sortBooksByAuthor(books);
            return this.author_down_books;
        } else if (direction === 'up_title') {
            this._sortBooksByTitle(books);
            return [].concat(this.title_down_books).reverse();
        } else if (direction === 'down_title') {
            this._sortBooksByTitle(books);
            return  this.title_down_books;
        } else {
            throw new Error('No sort direction detected');
        }
    }

    private _sortBooksByAuthor(books: Book[]) {
        if (this.author_down_books.length !== books.length) {
            this.author_down_books = books.sort(this._authorNameComparator);
        }
    }

    private _sortBooksByTitle(books: Book[]) {
        if (this.title_down_books.length !== books.length) {
            this.title_down_books = books.sort(this._titleComparator);
        }
    }

    private  _authorNameComparator(bookOne: Book, bookTwo: Book): number {
        return bookOne.author.name > bookTwo.author.name ? 1 : (bookOne.author.name < bookTwo.author.name ? -1 : 0);
    }

    private _titleComparator(bookOne: Book, bookTwo: Book): number {
        return bookOne.name > bookTwo.name ? 1 : (bookOne.name < bookTwo.name ? -1 : 0);
    }
}
