import { HttpProgressEvent } from '@angular/common/http';
import { SortService } from '../services/mergesort.service';

export class BooksRetrieverService {

    private _prelength = 0;
    private _initialNumberOfBooksToLoad: number;
    public booksSorted: Map<SortDirection, Book[]> = new Map<SortDirection, Book[]>();

    constructor(initialNumberOfBooksToLoad = 100) {
        if (this._initialNumberOfBooksToLoad > 1000) {
            throw new Error('Initial number of books to first load is to large');
        }
        this._initialNumberOfBooksToLoad = initialNumberOfBooksToLoad;
    }

    /**
     * Retrieves a {Book[]} from a the chunck recieved and returns an initial isplayed books array.
     * If partialText recieved is empty or undefined returns the same books and displayed books passed to the function
     *
     * @param event {HttpProgressEvent} from the reportProgress Http
     * @param books list of all books.
     * @param booksFiltered list of the first filtered books list to search
     * @param booksDisplayed list of the first books displayed in the page
     */
    public getChunckOfBooks<T extends HttpProgressEvent>(event: T, booksSorted: Map<SortDirection, Book[]>, booksDisplayed: Book[]): ChunckProcessedResponse {
        const partialReceivedTxt = (event as any).partialText;

        if (partialReceivedTxt && partialReceivedTxt.trim() !== '') {
            if (this._isFirstChunk(partialReceivedTxt, booksSorted.get('none'))) {
                const partialTextThatMakeSense = this._removeRightOffsetText(partialReceivedTxt);
                this._updateLengthWithProcessedOne(partialTextThatMakeSense);
                const newChunckOfBooks: Book[] = JSON.parse(partialTextThatMakeSense + (partialTextThatMakeSense.endsWith(']') ? '' : ']'))
                booksSorted.set('none', newChunckOfBooks);
                booksDisplayed = booksSorted.get('none').slice(0, this._initialNumberOfBooksToLoad);
            } else if (this._isMiddleChunk(partialReceivedTxt, booksSorted.get('none'))) {
                let partialTextThatMakeSense = '[' + this._removeLeftOffsetText(partialReceivedTxt);
                partialTextThatMakeSense = this._removeRightOffsetText(partialTextThatMakeSense);
                this._updateLengthWithProcessedOne(partialTextThatMakeSense);
                const newChunckOfBooks = JSON.parse(partialTextThatMakeSense + ']');
                booksSorted.set('none', booksSorted.get('none').concat(newChunckOfBooks));
                if (booksDisplayed.length < this._initialNumberOfBooksToLoad) {
                    booksDisplayed = booksSorted.get('none').slice(0, this._initialNumberOfBooksToLoad);
                }
            } else if (this._isLastChunk(partialReceivedTxt, booksSorted.get('none'))) {
                const partialTextThatMakeSense = '[' + this._removeLeftOffsetText(partialReceivedTxt);
                const newChunckOfBooks = JSON.parse(partialTextThatMakeSense);
                booksSorted.set('none', booksSorted.get('none').concat(newChunckOfBooks));
                this._prelength = 0;
            } else {
                console.warn('No detected txt partial text');
            }
        }
        return { books: booksSorted, booksDisplayed: booksDisplayed };
    }

    private _isFirstChunk(partialReceivedTxt: string, books: Book[]): boolean {
        return partialReceivedTxt.indexOf('[{') === 0 && books.length === 0;
    }

    private _isMiddleChunk(partialReceivedTxt: string, books: Book[]): boolean {
        return partialReceivedTxt.lastIndexOf('}]') === -1 && books.length > 0;
    }

    private _isLastChunk(partialReceivedTxt: string, books: Book[]): boolean {
        return partialReceivedTxt.lastIndexOf('}]') === partialReceivedTxt.length - 2 && books.length > 0;
    }

    private _removeLeftOffsetText(partialReceivedTxt: string): string {
        return partialReceivedTxt.substring(this._prelength + 1);
    }

    /**
     * If the string ends with }] nothing is removed as it is a correct ending
     *
     * @param partialReceivedTxt partial json string
     */
    private _removeRightOffsetText(partialReceivedTxt: string): string {
        return partialReceivedTxt.endsWith('}]') ? partialReceivedTxt : partialReceivedTxt.substring(0, partialReceivedTxt.lastIndexOf('},{') + 1);
    }

    private _updateLengthWithProcessedOne(processedText: string) {
        this._prelength += processedText.length;
    }
}
