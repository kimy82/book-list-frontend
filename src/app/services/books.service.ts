import { BookSettings } from '../../settings';
import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class BooksService {

    constructor(private _http: HttpClient) { }

    public getBooks(): Observable<HttpEvent<any>> {
        return this._http.request(new HttpRequest('GET', BookSettings.backendUrl, { reportProgress: true, responseType: 'text'}));
    }

}
