import BookSettings from '../settings';

BookSettings.backendUrl = 'http://localhost:3000';

export const environment = {
  production: false
};
