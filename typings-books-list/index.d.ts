type Gender = 'male' | 'female';
type Genre = 'Satire' | 'Science fiction' | 'Drama' | 'Action' | 'Romance' | 'Mystery' | 'Horror' | 'Finance';

interface Author {
    gender: Gender;
    name: string;
}

interface Book {
    name: string;
    author: Author;
    genre: Genre;
    published: Date;
}

interface ChunckProcessedResponse {
    books:  Map<SortDirection, Book[]>;
    booksDisplayed: Book[];
}

type SortDirection = 'up_title' | 'down_title' | 'up_author' | 'down_author' | 'none';