# BooksList

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.2

    - It is not prepared for production Environments.
    - It doesn't have e2e testing as I think it is not really necessary for the code challenge.

##Environments

Only with dev environment. Check that in `environemnts/environment.ts`

## Run development server

Run _ng serve_ in order to start the dev server and navigate to `http://localhost:4200/`

## Run with docker

From root directory run _docker build -t example/book-list-frontend ._ in order to build docker container and then run _docker run -it --rm -p 4200:4200 example/book-list-frontend_ to start it. 
* NOTE: In linux machines there is no need to change the connection to backend books server, however, windows < 10 it should be change in environemt.ts. Swap locahost for 192.168.99.10X.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Weak Points
    - The books retriever service might not work properly if the Book Object would have an array within as the service relies on the fact that the begining of the chunk is _[{_ and the end is _}]_.